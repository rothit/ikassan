package se.birdcroft.ikassan.ui.adapters;

import java.util.List;

import se.birdcroft.ikassan.R;
import se.birdcroft.ikassan.ui.utils.listview.elements.ListItem;
import se.birdcroft.ikassan.ui.utils.listview.elements.SectionListItem;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public abstract class SectionListAdapter extends ArrayAdapter<ListItem> {
	private List<ListItem> mListItems;
	private LayoutInflater mLayout;
	private int mElementLayoutRes = 0;
	private int mSectionLayoutRes = 0;
	
	/**
	 * @return the listItems
	 */
	public List<ListItem> getListItems() {
		return mListItems;
	}

	/**
	 * @return the section view layout
	 */
	public View getSectionLayout() {
		return mLayout.inflate(mSectionLayoutRes, null);
	}
	
	/**
	 * @return the section view layout
	 */
	public View getElementLayout() {
		return mLayout.inflate(mElementLayoutRes, null);
	}
	
	/**
	 * 
	 * @param context
	 * @param listItems
	 * @param elementLayoutRes
	 */
	public SectionListAdapter(Context context, List<ListItem> listItems, int elementLayoutRes) {
		super(context,0, listItems);
		this.mListItems = listItems;
		this.mLayout = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mElementLayoutRes = elementLayoutRes;
		this.mSectionLayoutRes = R.layout.list_section;
	}
	
	/**
	 * 
	 * @param context
	 * @param listItems
	 * @param elementLayoutRes
	 * @param sectionLayoutRes
	 */
	public SectionListAdapter(Context context, List<ListItem> listItems, int elementLayoutRes, int sectionLayoutRes) {
		this(context, listItems, elementLayoutRes);
		
		this.mListItems = listItems;
		this.mLayout = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mElementLayoutRes = elementLayoutRes;
		this.mSectionLayoutRes = sectionLayoutRes;
	}

	
	/* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ListItem listItem = mListItems.get(position);
		View view = null;
		if(listItem != null){
			if(listItem.isSection()){
				view = insertSection(getSectionLayout(), listItem);
			} else {
				view = insertItem(getElementLayout(), position);
			}
		}
		
		return view;
	}
	
	protected View insertSection(View view, ListItem item){
		SectionListItem section = (SectionListItem)item;
		
		view.setOnClickListener(null);
		view.setOnLongClickListener(null);
		view.setLongClickable(false);
		
		TextView title = (TextView) view.findViewById(R.id.list_item_section_text);
		title.setText(section.getTitle());
		return view;
	}
	
	abstract protected View insertItem(View view, int position);
}
