package se.birdcroft.ikassan.ui.adapters;

import java.util.List;

import se.birdcroft.ikassan.R;
import se.birdcroft.ikassan.ui.utils.listview.elements.ListItem;
import se.birdcroft.ikassan.ui.utils.listview.elements.SectionListItem;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

public class DetailsListAdapter extends SectionListAdapter {

	public DetailsListAdapter(Context context){
		super(context, null, 0, 0);
	}
	
	public DetailsListAdapter(Context context, List<ListItem> listItems){
		super(context, listItems, R.layout.list_details_item, R.layout.list_details_header);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected View insertSection(View view, ListItem item) {
		SectionListItem section = (SectionListItem)item;
		
		view.setOnClickListener(null);
		view.setOnLongClickListener(null);
		view.setLongClickable(false);
		
		TextView title = (TextView) view.findViewById(R.id.tv_grp_headerTitle);
		TextView summery = (TextView) view.findViewById(R.id.tv_grp_headerTotal);
		
		title.setText(section.getTitle());
		summery.setText(section.getSummary());
		
		return view;
	}

	@Override
	protected View insertItem(View view, int position) {
		ListItem listItem = getListItems().get(position);
		
		final TextView itemTitle = (TextView) view.findViewById(R.id.tv_grp_itemTitle);
		final TextView itemSummary = (TextView) view.findViewById(R.id.tv_grp_itemTotal);
		
		if(itemTitle != null){
			itemTitle.setText(listItem.getTitle());
		}
		
		if(itemSummary != null){
			itemSummary.setText(listItem.getSummary());
		}
		
		return view;
	}

}
