package se.birdcroft.ikassan.ui.adapters;

import java.util.List;

import se.birdcroft.ikassan.R;
import se.birdcroft.ikassan.ui.utils.listview.elements.ListItem;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

@SuppressLint("ViewConstructor")
public class ActivityListAdapter extends SectionListAdapter {
	
	public ActivityListAdapter(Context context, List<ListItem> activities) {
		super(context, activities, R.layout.main_list_item);
	}

	@Override
	protected View insertItem(View view, int position) {
		ListItem listItem = getListItems().get(position);
		
		final TextView itemTitle = (TextView) view.findViewById(R.id.list_item_title);
		final TextView itemSummary = (TextView) view.findViewById(R.id.list_item_summary);
		
		if(itemTitle != null){
			itemTitle.setText(listItem.getTitle());
		}
		
		if(itemSummary != null){
			itemSummary.setText(listItem.getSummary());
		}
		
		return view;
	}
	
	
}
