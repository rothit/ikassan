package se.birdcroft.ikassan.ui.adapters;

import java.util.List;

import se.birdcroft.ikassan.R;
import se.birdcroft.ikassan.ui.utils.listview.elements.ListItem;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

@SuppressLint("ViewConstructor")
public class CategoryListAdapter extends SectionListAdapter {

	public CategoryListAdapter(Context context, List<ListItem> listItems) {
		super(context, listItems, R.layout.list_details_item);
	}

	@Override
	protected View insertItem(View view, int position) {
		ListItem currentItem = getListItems().get(position);
		
		TextView tvCategoryTitle = (TextView) view.findViewById(R.id.tv_grp_itemTitle);
		
		if(tvCategoryTitle != null) {
			tvCategoryTitle.setText(currentItem.getTitle());
		}
		
		return view;
	}

}
