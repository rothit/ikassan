package se.birdcroft.ikassan.ui.adapters;

import java.util.List;

import se.birdcroft.ikassan.R;
import se.birdcroft.ikassan.ui.utils.listview.elements.ListItem;
import se.birdcroft.ikassan.ui.utils.listview.elements.OutgoingListItem;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

@SuppressLint("ViewConstructor")
public class OutgoingListAdapter extends SectionListAdapter {

	public OutgoingListAdapter(Context context, List<ListItem> listItems) {
		super(context, listItems, R.layout.list_details_item);
	}

	@Override
	protected View insertItem(View view, int position) {
		ListItem currentItem = (OutgoingListItem) getListItems().get(position);
		
		TextView tvTitle = (TextView) view.findViewById(R.id.tv_grp_itemTitle);
		TextView tvTotal = (TextView) view.findViewById(R.id.tv_grp_itemTotal);
		
		if(tvTitle != null){
			tvTitle.setText(currentItem.getTitle());
		}
		
		if(tvTotal != null){
			tvTotal.setText(currentItem.getSummary());
		}
		
		return view;
	}

}
