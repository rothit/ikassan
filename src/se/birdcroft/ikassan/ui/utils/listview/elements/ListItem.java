package se.birdcroft.ikassan.ui.utils.listview.elements;

public interface ListItem {
	
	public String getTitle();
	public void setTitle(String newTitle);
	
	public String getSummary();
	public void setSummary(String newSummary);
	
	public boolean isSection();
	public void execute();
}
