package se.birdcroft.ikassan.ui.utils.listview.elements;

import android.content.Context;
import android.content.Intent;

/**
 * This list element class handles the activate of a activity
 * that the user have choose.
 * 
 * @author Mikeal Ekroth
 * @version 2014-02-25
 */
public class ActivityListItem implements ListItem {
	
	private Context mCurrentContext;
	private Class<?> mExecuteActivity;
	private String mTitle;
	private String mSummary;
	
	/**
	 * @param mCurrentContext
	 * @param mExecuteActivity
	 * @param mTitle
	 * @param mSummary
	 */
	public ActivityListItem(Context mCurrentContext, Class<?> mExecuteActivity,
			String mTitle, String mSummary) {
		super();
		this.mCurrentContext = mCurrentContext;
		this.mExecuteActivity = mExecuteActivity;
		this.mTitle = mTitle;
		this.mSummary = mSummary;
	}
	

	@Override
	public String getTitle() {
		return mTitle;
	}

	@Override
	public void setTitle(String newTitle) {
		this.mTitle = newTitle;
	}
	
	@Override
	public String getSummary() {
		return mSummary;
	}

	@Override
	public void setSummary(String newSummary) {
		this.mSummary = newSummary;
	}
	
	//See comments of the interface ListItem
	@Override
	public boolean isSection() {
		// TODO Auto-generated method stub
		return false;
	}
	
	//See comments of the interface ListItem
	@Override
	public void execute() {
		mCurrentContext.startActivity(new Intent(mCurrentContext, mExecuteActivity));
	}
}
