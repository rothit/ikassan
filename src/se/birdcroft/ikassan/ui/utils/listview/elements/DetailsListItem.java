package se.birdcroft.ikassan.ui.utils.listview.elements;

public class DetailsListItem implements ListItem {
	
	private String mTitle;
	private String mSummery;
	
	
	
	public DetailsListItem(String title, String summery) {
		super();
		this.mTitle = title;
		this.mSummery = summery;
	}

	@Override
	public String getTitle() {
		return mTitle;
	}

	@Override
	public void setTitle(String newTitle) {
		this.mTitle = newTitle;
	}

	@Override
	public String getSummary() {
		return mSummery;
	}

	@Override
	public void setSummary(String newSummary) {
		this.mSummery = newSummary;
	}

	@Override
	public boolean isSection() {
		return false;
	}

	@Override
	public void execute() {
		throw new UnsupportedOperationException("This method is not supported for this element.");
	}

}
