package se.birdcroft.ikassan.ui.utils.listview.elements;

public class CategoryListItem implements ListItem {

	private String mCategoryTitle;
	
	public CategoryListItem(String categoryTitle) {
		super();
		this.mCategoryTitle = categoryTitle;
	}

	@Override
	public String getTitle() {
		return mCategoryTitle;
	}

	@Override
	public void setTitle(String newTitle) {
		this.mCategoryTitle = newTitle;
	}

	@Override
	public String getSummary() {
		return null;
	}

	@Override
	public void setSummary(String newSummary) {
		throw new UnsupportedOperationException("This method is not supported for this element.");
	}

	@Override
	public boolean isSection() {
		return false;
	}

	@Override
	public void execute() {
		throw new UnsupportedOperationException("This method is not supported for this element.");
	}

}
