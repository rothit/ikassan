package se.birdcroft.ikassan.ui.utils.listview.elements;

import android.widget.CheckBox;

public class ProductListItem implements MarkebleListItem {
	
	private String title;
	private String summary;
	private boolean selectedItem = false;
	
	/**
	 * If the item is selected or not
	 * 
	 * @return the selectedItem (true/false)
	 */
	public boolean isSelectedItem() {
		return selectedItem;
	}

	@Override
	public String getTitle() {
		// TODO Auto-generated method stub
		return title;
	}

	@Override
	public void setTitle(String newTitle) {
		this.title = newTitle;
	}

	@Override
	public String getSummary() {
		return summary;
	}

	@Override
	public void setSummary(String newSummary) {
		
	}
	
	@Override
	public boolean isSection() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void execute() {
		throw new UnsupportedOperationException("This method is not supported for this element.");
	}
	
	@Override
	public void execute(CheckBox cbSelected) {
		this.selectedItem = cbSelected.isChecked();
	}
}
