package se.birdcroft.ikassan.ui.utils.listview.elements;

public class SectionListItem implements ListItem {

	private String mTitle;
	private String mSummery;
	
	public SectionListItem(String title) {
		super();
		this.mTitle = title;
		this.mSummery = "";
	}

	public SectionListItem(String title, String summary) {
		super();
		this.mTitle = title;
		this.mSummery = summary;
	}
	
	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String title) {
		this.mTitle = title;
	}

	@Override
	public boolean isSection() {
		return true;
	}

	@Override
	public void execute() {
		throw new UnsupportedOperationException("This method is not supported for this element.");
	}

	@Override
	public String getSummary() {
		return mSummery;
	}

	@Override
	public void setSummary(String newSummary) {
		this.mSummery = newSummary;
	}

}
