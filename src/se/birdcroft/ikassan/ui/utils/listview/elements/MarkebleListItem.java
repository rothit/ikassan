package se.birdcroft.ikassan.ui.utils.listview.elements;

import android.widget.CheckBox;

public interface MarkebleListItem extends ListItem {
	void execute(CheckBox cbSelected);
}
