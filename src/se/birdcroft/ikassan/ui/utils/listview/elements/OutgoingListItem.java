package se.birdcroft.ikassan.ui.utils.listview.elements;


public class OutgoingListItem implements ListItem {
	
	private String mSummery;
	private String mTitle;
	
	
	/**
	 * 
	 * @param title
	 * @param summery
	 */
	public OutgoingListItem(String title, String summery) {
		super();
		this.mSummery = summery;
		this.mTitle = title;
	}

	@Override
	public String getTitle() {
		return mTitle;
	}

	@Override
	public void setTitle(String newTitle) {
		this.mTitle = newTitle;
	}

	@Override
	public String getSummary() {
		// TODO Auto-generated method stub
		return mSummery;
	}

	@Override
	public void setSummary(String newSummary) {
		this.mSummery = newSummary;
	}

	@Override
	public boolean isSection() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void execute() {
		
	}

}
