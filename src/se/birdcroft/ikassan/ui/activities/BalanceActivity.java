package se.birdcroft.ikassan.ui.activities;

import se.birdcroft.ikassan.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class BalanceActivity extends Activity {

	private TextView tvCurrentBalance;
	private Button btnShowPeriod;
	
	private void loadActivityContents() {
		tvCurrentBalance = (TextView) findViewById(R.id.tv_balance_tot);
		btnShowPeriod = (Button) findViewById(R.id.btn_show_period);
		
		btnShowPeriod.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				startActivity(new Intent(BalanceActivity.this, PeriodActivity.class));
			}
		});
		
		loadBalanceData();
	}
	
	private void loadBalanceData(){
		tvCurrentBalance.setText("10 000 kr");
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_balance);
		loadActivityContents();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.balance, menu);
		return true;
	}

}
