package se.birdcroft.ikassan.ui.activities;

import java.util.ArrayList;
import java.util.List;

import se.birdcroft.ikassan.R;
import se.birdcroft.ikassan.ui.adapters.ActivityListAdapter;
import se.birdcroft.ikassan.ui.adapters.SectionListAdapter;
import se.birdcroft.ikassan.ui.utils.listview.elements.ActivityListItem;
import se.birdcroft.ikassan.ui.utils.listview.elements.ListItem;
import se.birdcroft.ikassan.ui.utils.listview.elements.SectionListItem;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemClickListener{
	
	private List<ListItem> mActivities;
	private ListView lvActivityList;
	private String[] mHeaders, mActTitles, mActSummary;
	
	private void initActivityList() {
		mActivities = new ArrayList<ListItem>();
		
		mHeaders = getResources().getStringArray(R.array.main_sections_array);
		mActTitles = getResources().getStringArray(R.array.main_titles_array);
		mActSummary = getResources().getStringArray(R.array.main_summaries_array);
		
		lvActivityList = (ListView)findViewById(R.id.lv_main_activities);
		
		mActivities.add(new SectionListItem(mHeaders[0]));
		mActivities.add(new ActivityListItem(MainActivity.this, null, mActTitles[0], mActSummary[0]));
		mActivities.add(showRegisterIncome());
		mActivities.add(new ActivityListItem(MainActivity.this, RegistrateOutgoingActivity.class, mActTitles[2], mActSummary[2]));
		mActivities.add(new ActivityListItem(MainActivity.this, RegistrateCategoryActivity.class, mActTitles[3], mActSummary[3]));
		mActivities.add(new SectionListItem(mHeaders[1]));
		mActivities.add(new ActivityListItem(MainActivity.this, BalanceActivity.class, mActTitles[4], mActSummary[4]));
		mActivities.add(new ActivityListItem(MainActivity.this, PeriodActivity.class, mActTitles[5], mActSummary[5]));
		
		SectionListAdapter sectionAdapter = new ActivityListAdapter(this, mActivities);
		
		lvActivityList.setAdapter(sectionAdapter);
		lvActivityList.setOnItemClickListener(this);
	}
	
	private void displayIncomeDialog() {
		final Dialog incomeDialog = new Dialog(this);
		incomeDialog.setContentView(R.layout.dialog_income);
		incomeDialog.setTitle(getString(R.string.dialog_income_title));
		
		Button dialog_btnSave = (Button) incomeDialog.findViewById(R.id.btn_income_save);
		Button dialog_btnCancel = (Button) incomeDialog.findViewById(R.id.btn_income_cancel);
		final EditText et_total_income = (EditText) incomeDialog.findViewById(R.id.et_income_total);
		
		dialog_btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				addIncome(et_total_income.getText().toString());
				incomeDialog.dismiss();
				et_total_income.setText("");
			}
		});
		
		dialog_btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				incomeDialog.dismiss();
				et_total_income.setText("");
			}
		});
		
		incomeDialog.show();
	}
	
	private void addIncome(String totIncome) {
		Toast.makeText(this, "Your income : " + totIncome + " is added !" , Toast.LENGTH_SHORT).show();
	}
	
	private ListItem showRegisterIncome() {
		return new ListItem() {
			
			@Override
			public void setTitle(String newTitle) { }
			
			@Override
			public void setSummary(String newSummary) { }
			
			@Override
			public boolean isSection() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public String getTitle() {
				// TODO Auto-generated method stub
				return mActTitles[1];
			}
			
			@Override
			public String getSummary() {
				// TODO Auto-generated method stub
				return mActSummary[1];
			}
			
			@Override
			public void execute() {
				displayIncomeDialog();
			}
		};
	}

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initActivityList();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
		ListItem item = (ListItem)mActivities.get(position);
		
		if(item != null){
			item.execute();
		}
		
		Toast.makeText(this, "You clicked " + item.getTitle() , Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.action_settings:
			startActivity(new Intent(this, SettingsActivity.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
