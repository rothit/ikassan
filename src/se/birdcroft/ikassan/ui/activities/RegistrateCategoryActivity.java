package se.birdcroft.ikassan.ui.activities;

import java.util.List;

import se.birdcroft.ikassan.data.database.CategoryDataSource;
import se.birdcroft.ikassan.data.entities.Category;
import se.birdcroft.ikassan.R;
import android.app.Dialog;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;

public class RegistrateCategoryActivity extends ListActivity {
	
	private CategoryDataSource _DataSource;
	private List<Category> _Categories;
	
	// UI components
	private EditText inputTitle;
	private Dialog _CategoryDialog;
	private Button btnShowAddDialog;
	private ArrayAdapter<Category> _ListAdapter;
	
	private void initViews(){
		initAddCategoryDialog();
		
		inputTitle = (EditText) _CategoryDialog.findViewById(R.id.et_category_title_field);
		btnShowAddDialog = (Button)findViewById(R.id.btn_add_category);
		btnShowAddDialog.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				_CategoryDialog.show();
			}
		});
	}
	
	private void initAddCategoryDialog() {
		_CategoryDialog = new Dialog(this);
		_CategoryDialog.setContentView(R.layout.dialog_add_category);
		_CategoryDialog.setTitle(getString(R.string.dialog_add_category_title));
		
		Button dialog_btnSave = (Button) _CategoryDialog.findViewById(R.id.btn_save_category);
		Button dialog_btnCancel = (Button) _CategoryDialog.findViewById(R.id.btn_cancel_category);
		
		dialog_btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				saveCategory(inputTitle.getText().toString());
				_CategoryDialog.dismiss();
				reset();
			}
		});
		
		dialog_btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				_CategoryDialog.dismiss();
				reset();
			}
		});
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		_DataSource = new CategoryDataSource(this);
		_DataSource.open();
		
		_Categories = _DataSource.findAll();
		setContentView(R.layout.activity_registrate_category);
		initViews();
		_ListAdapter = new ArrayAdapter<Category>(this, android.R.layout.simple_list_item_1, _Categories);
		setListAdapter(_ListAdapter);
	}

	private void saveCategory(String name){
		Category category = new Category(name);
		
		long id = _DataSource.save(category);
		
		updateAdapter(_DataSource.findById(id));
	}
	
	private void updateCategory(Category editedCategory){
		long catId = _DataSource.save(editedCategory);
		
		updateAdapter(_DataSource.findById(catId));
	}
	
	private void updateAdapter(Category cat){
		if (cat == null) { return; }
		
		_Categories.add(cat);
		
		//updates the list-view with new data
		_ListAdapter.notifyDataSetChanged();
	}
	
	private void reset(){
		inputTitle.setText("");
	}
}
