package se.birdcroft.ikassan.ui.activities;

import java.util.ArrayList;
import java.util.List;

import se.birdcroft.ikassan.R;
import se.birdcroft.ikassan.ui.adapters.OutgoingListAdapter;
import se.birdcroft.ikassan.ui.adapters.SectionListAdapter;
import se.birdcroft.ikassan.ui.utils.listview.elements.ListItem;
import se.birdcroft.ikassan.ui.utils.listview.elements.OutgoingListItem;
import se.birdcroft.ikassan.ui.utils.listview.elements.SectionListItem;
import android.app.Dialog;
import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by MickeEkroth on 2014-02-28.
 */
public class RegistrateOutgoingActivity extends ListActivity {
	
	
	private List<ListItem> mOutgoings;
    private Dialog mOutgoingDialog;
    private EditText et_title, et_total;
    private SectionListAdapter mOutgoingAdapter;
    private Button btnShowAddDialog;

    private void initViews(){
    	initAddOutgoingDialog();
		et_title = (EditText) mOutgoingDialog.findViewById(R.id.et_outgoing_name_field);
		et_total = (EditText) mOutgoingDialog.findViewById(R.id.et_outgoing_total_field);
    	mOutgoings = new ArrayList<ListItem>();
    	loadCurrentOutgoings();
    	
    	btnShowAddDialog = (Button) findViewById(R.id.btn_add_outgoings);
    	
    	btnShowAddDialog.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				mOutgoingDialog.show();
			}
		});
    }
    
	private void loadCurrentOutgoings() {
		mOutgoings.add(new SectionListItem(getString(R.string.conf_outgoing_header)));
		mOutgoings.add(new OutgoingListItem("Hyra", "4000"+" kr"));
		mOutgoings.add(new OutgoingListItem("Mobil", "499"+" kr"));
		mOutgoings.add(new OutgoingListItem("Sparande", "1000"+" kr"));
	}

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrate_outgoings);
        initViews();
        mOutgoingAdapter = new OutgoingListAdapter(this, mOutgoings);
        setListAdapter(mOutgoingAdapter);
    }
    
	private void initAddOutgoingDialog() {
		mOutgoingDialog = new Dialog(this);
		mOutgoingDialog.setContentView(R.layout.dialog_add_outgoing);
		mOutgoingDialog.setTitle(getString(R.string.dialog_add_outgoing_title));
		
		Button dialog_btnSave = (Button) mOutgoingDialog.findViewById(R.id.btn_save_outgoing);
		Button dialog_btnCancel = (Button) mOutgoingDialog.findViewById(R.id.btn_cancel_outgoing);
		
		dialog_btnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String title = et_title.getText().toString();
				String total = et_total.getText().toString();
				
				addOutgoing(title, total);
				mOutgoingDialog.dismiss();
				reset();
			}
		});
		
		dialog_btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				mOutgoingDialog.dismiss();
				reset();
			}
		});
	}
	
	private void addOutgoing(String title, String total) {
		mOutgoings.add(new OutgoingListItem(title, total +" kr"));
		mOutgoingAdapter.notifyDataSetChanged();
	}
	
	private void reset(){
		et_title.setText("");
		et_total.setText("");
	}
}