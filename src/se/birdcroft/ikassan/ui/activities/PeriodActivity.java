package se.birdcroft.ikassan.ui.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.birdcroft.ikassan.R;
import se.birdcroft.ikassan.ui.adapters.DetailsListAdapter;
import se.birdcroft.ikassan.ui.utils.listview.elements.DetailsListItem;
import se.birdcroft.ikassan.ui.utils.listview.elements.ListItem;
import se.birdcroft.ikassan.ui.utils.listview.elements.SectionListItem;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("UseSparseArrays")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class PeriodActivity extends Activity {
	
	private Map<Integer, TextView> mTextViews;
	private ListView lvDetails;
	private List<ListItem> mDetailsData;
	private Button btnShowDetails;
	
	//private Period mCurrentPeriod;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_period);
		// Show the Up button in the action bar.
		loadActivityContents();
	}

	private void loadActivityContents() {
		initTextViewMap();
		lvDetails = (ListView) findViewById(R.id.lv_details);
		
		btnShowDetails = (Button) findViewById(R.id.btn_period_details);
		
		btnShowDetails.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				switch (lvDetails.getVisibility()){
					case ExpandableListView.INVISIBLE:
						lvDetails.setVisibility(ListView.VISIBLE);
					break;
					default:
						lvDetails.setVisibility(ListView.INVISIBLE);
						break;
				}
			}
		});
		
		//Gets the static strings from string resource
		mTextViews.get(R.id.tv_period_income).setText(getText(R.string.period_income));
		mTextViews.get(R.id.tv_period_outgoings).setText(getText(R.string.period_outgoings));
		mTextViews.get(R.id.tv_period_balance).setText(getText(R.string.period_balance));
		
		loadPeriodData();
	}

	private void loadPeriodData() {
		
		mTextViews.get(R.id.tv_period_balance_tot).setText("10 000,00 kr");
		mTextViews.get(R.id.tv_period_in_total).setText("15 0000,00 kr");
		mTextViews.get(R.id.tv_period_out_total).setText("5 000,00 kr");
		mTextViews.get(R.id.tv_period_month).setText("FEBRUARI");
		mTextViews.get(R.id.tv_period_year).setText("2014");
		
		loadDetailsData();
	}

	private void loadDetailsData() {
		prepareListData();
		
		lvDetails.setAdapter(new DetailsListAdapter(this, mDetailsData));
		
	}

	private void prepareListData() {
		mDetailsData = new ArrayList<ListItem>();
		List<ListItem> sections = getSections();
		//Outgoings per month
		mDetailsData.add(sections.get(0));
		mDetailsData.add(new DetailsListItem("Hyra", "4000"));
		mDetailsData.add(new DetailsListItem("Mobil", "499"));
		mDetailsData.add(new DetailsListItem("Sparande", "1000"));
		mDetailsData.get(0).setSummary("5999");
		
		//Individual outgoings for the month
		mDetailsData.add(sections.get(1));
		mDetailsData.add(new DetailsListItem("Mat", "4000"));
		mDetailsData.add(new DetailsListItem("Hygien", "499"));
		mDetailsData.add(new DetailsListItem("N??jen", "5000"));
		mDetailsData.get(4).setSummary("9999");
	}

	private List<ListItem> getSections() {
		List<ListItem> list = new ArrayList<ListItem>();
		
		for(String section : getResources().getStringArray(R.array.period_details_sections)){
			list.add(new SectionListItem(section, ""));
		}
		
		return list;
	}

	private void initTextViewMap() {
		mTextViews = new HashMap<Integer, TextView>();
		
		mTextViews.put(R.id.tv_period_month, (TextView) findViewById(R.id.tv_period_month));
		mTextViews.put(R.id.tv_period_year, (TextView) findViewById(R.id.tv_period_year));
		mTextViews.put(R.id.tv_period_income, (TextView) findViewById(R.id.tv_period_income));
		mTextViews.put(R.id.tv_period_outgoings, (TextView) findViewById(R.id.tv_period_outgoings));
		mTextViews.put(R.id.tv_period_in_total, (TextView) findViewById(R.id.tv_period_in_total));
		mTextViews.put(R.id.tv_period_out_total, (TextView) findViewById(R.id.tv_period_out_total));
		mTextViews.put(R.id.tv_period_balance, (TextView) findViewById(R.id.tv_period_balance));
		mTextViews.put(R.id.tv_period_balance_tot, (TextView) findViewById(R.id.tv_period_balance_tot));
		
	}
}

