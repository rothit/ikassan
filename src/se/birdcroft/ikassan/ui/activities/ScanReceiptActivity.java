package se.birdcroft.ikassan.ui.activities;

import se.birdcroft.ikassan.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class ScanReceiptActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_scan_receipt);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.scan_receipt, menu);
		return true;
	}

}
