package se.birdcroft.ikassan.helpers;

public enum BalanceStatus {
	GOOD("#659C0F"),
	OK("#FA8C14"),
	BAD("#C70007");
	
	private String mColorHex;
	
	private BalanceStatus(String colorHex){
		this.mColorHex = colorHex;
	}
	
	public String toString(){
		return this.mColorHex;
	}

	public static String getStatusColor(double balance_total) {
		
		return null;
	}
}
