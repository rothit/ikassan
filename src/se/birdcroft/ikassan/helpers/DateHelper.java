package se.birdcroft.ikassan.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

public final class DateHelper {
	
	public static Date conertToDate(String inputDate){
		SimpleDateFormat formatter = new SimpleDateFormat();
		try {
			return formatter.parse(inputDate);
		} catch (Exception e) {
			return null;
		}
	}
}
