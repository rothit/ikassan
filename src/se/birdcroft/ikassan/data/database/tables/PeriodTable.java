/**
 * 
 */
package se.birdcroft.ikassan.data.database.tables;

/**
 * @author MickeEkroth
 *
 */
public class PeriodTable extends Table {
	public static final String NAME = "period";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_PERIOD = "period";
	public static final String COLUMN_INCOME = "income";
	
	protected static final String CREATE_TABLE = "CREATE TABLE "
													+ NAME + " ( "
													+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
													+ COLUMN_PERIOD + " DATETIME NOT NULL DEFAULT CURRENT_DATE, "
													+ COLUMN_INCOME + " REAL NOT NULL "
													+ ");";
	@Override
	public String create() {
		return CREATE_TABLE;
	}

	@Override
	public String getTableName() {
		return NAME;
	}
}
