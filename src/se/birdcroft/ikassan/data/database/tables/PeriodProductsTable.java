/**
 * 
 */
package se.birdcroft.ikassan.data.database.tables;

/**
 * @author MickeEkroth
 *
 */
public class PeriodProductsTable extends Table {
	
	public static final String NAME = "period_products";
	public static final String COLUMN_PERIOD_ID = "period_id";
	public static final String COLUMN_PRODUCT_ID = "product_id";
	protected static final String CREATE_TABLE = "CREATE TABLE "
													+ NAME + " ("
													+ COLUMN_PERIOD_ID + " INTEGER NOT NULL, "
													+ COLUMN_PRODUCT_ID + " INTEGER NOT NULL, "
													+ "PRIMARY KEY ("+ COLUMN_PERIOD_ID +", "+ COLUMN_PRODUCT_ID +")"
													+ ");";
	
	/* (non-Javadoc)
	 * @see se.birdcroft.ikassan.data.database.Table#create()
	 */
	@Override
	public String create() {
		// TODO Auto-generated method stub
		return CREATE_TABLE;
	}

	/* (non-Javadoc)
	 * @see se.birdcroft.ikassan.data.database.Table#getTableName()
	 */
	@Override
	public String getTableName() {
		// TODO Auto-generated method stub
		return NAME;
	}

}
