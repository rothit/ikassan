package se.birdcroft.ikassan.data.database.tables;

public class ProductTable extends Table {
	public static final String NAME = "product";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_PRICE = "price";
	public static final String COLUMN_CATEGORY = "category";
	
	protected static final String CREATE_TABLE = "CREATE TABLE "
													+ NAME + " ("
													+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
													+ COLUMN_NAME + " TEXT NOT NULL, "
													+ COLUMN_PRICE + " REAL NOT NULL, "
													+ COLUMN_CATEGORY + " INTEGER NOT NULL "
													+ ");";

	@Override
	public String create() {
		// TODO Auto-generated method stub
		return CREATE_TABLE;
	}

	@Override
	public String getTableName() {
		// TODO Auto-generated method stub
		return NAME;
	}
}
