package se.birdcroft.ikassan.data.database.tables;

public class CategoryTable extends Table {
	public static final String NAME = "category";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_IS_ACTIVE = "is_active";
	protected static final String CREATE_TABLE = "CREATE TABLE "
													+ NAME + " ( "
													+ COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
													+ COLUMN_NAME + " TEXT NOT NULL, "
													+ COLUMN_IS_ACTIVE + " INTEGER NOT NULL DEFAULT 1"
													+ ");";
	
	@Override
	public String getTableName() {
		return NAME;
	}
	@Override
	public String create() {
		return CREATE_TABLE;
	}
}