/**
 * 
 */
package se.birdcroft.ikassan.data.database.tables;

/**
 * @author MickeEkroth
 *
 */
public class PeriodOutgoingsTable extends Table {

	public static final String NAME = "period_outgoings";
	public static final String COLUMN_PERIOD_ID = "period_id";
	public static final String COLUMN_OUTGOING_ID = "outgoing_id";
	protected static final String CREATE_TABLE = "CREATE TABLE "
													+ NAME + " ( "
													+ COLUMN_PERIOD_ID + " INTEGER NOT NULL, "
													+ COLUMN_OUTGOING_ID + " INTEGER NOT NULL, "
													+ "PRIMARY KEY(" + COLUMN_PERIOD_ID + ", " + COLUMN_OUTGOING_ID + ")"
													+ ");";
	
	/* (non-Javadoc)
	 * @see se.birdcroft.ikassan.data.database.Table#create()
	 */
	@Override
	public String create() {
		return CREATE_TABLE;
	}

	/* (non-Javadoc)
	 * @see se.birdcroft.ikassan.data.database.Table#getTableName()
	 */
	@Override
	public String getTableName() {
		return NAME;
	}

}
