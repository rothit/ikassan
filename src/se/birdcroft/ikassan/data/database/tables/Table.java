package se.birdcroft.ikassan.data.database.tables;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public abstract class Table {
	
	public static void onCreate(SQLiteDatabase database, String tblCreateQuery) {
		database.execSQL(tblCreateQuery);
	}
	
	public static void onUpdate(SQLiteDatabase database, String table, int oldVersion, int newVersion){
		Log.w(Table.class.getSimpleName(), 
				"Upgrading the database from version "
				+ oldVersion + " to " + newVersion
				+ ", which will destroy all old data.");
		
		database.execSQL("DROP IF EXISTS " + table);		
	}

	public abstract String create();
	
	public abstract String getTableName();
}
