/**
 * 
 */
package se.birdcroft.ikassan.data.database;

import se.birdcroft.ikassan.data.database.tables.CategoryTable;
import se.birdcroft.ikassan.data.database.tables.OutgoingTable;
import se.birdcroft.ikassan.data.database.tables.PeriodOutgoingsTable;
import se.birdcroft.ikassan.data.database.tables.PeriodProductsTable;
import se.birdcroft.ikassan.data.database.tables.PeriodTable;
import se.birdcroft.ikassan.data.database.tables.ProductTable;

/**
 * @author MickeEkroth
 *
 */
public enum SelectQueries {
	PERIOD_DETAILS("SELECT c."+ CategoryTable.COLUMN_NAME + ", "
				   + "SUM(p." + ProductTable.COLUMN_PRICE + ") AS total "
				   + "FROM " + CategoryTable.NAME + " AS c, "
				   + ProductTable.NAME +" AS p, "
				   + PeriodProductsTable.NAME + "AS pp " 
				   + "WHERE c." + CategoryTable.COLUMN_ID + " = "+ ProductTable.COLUMN_CATEGORY 
				   + " AND pp."+ PeriodProductsTable.COLUMN_PRODUCT_ID + " = " + ProductTable.COLUMN_ID
				   + " AND pp." + PeriodProductsTable.COLUMN_PERIOD_ID + " = ? "
				   + "GROUP BY p."+ ProductTable.COLUMN_CATEGORY), 
	
	PERIOD_OUTGOING("SELECT o."+ OutgoingTable.COLUMN_NAME
					+ ", o."+ OutgoingTable.COLUMN_PRICE
					+ ", SUM(o." + OutgoingTable.COLUMN_PRICE+") AS total"
					+ "FROM "+ OutgoingTable.NAME + " AS o, "
					+ PeriodTable.NAME +" AS p, "
					+ PeriodOutgoingsTable.NAME +" AS po"
					+ "WHERE o."+ OutgoingTable.COLUMN_ID + " = po." + PeriodOutgoingsTable.COLUMN_OUTGOING_ID
					+ "AND p." + PeriodTable.COLUMN_ID + " = po." + PeriodOutgoingsTable.COLUMN_PERIOD_ID
					+ "AND p."+ PeriodTable.COLUMN_ID + " = ?"
					+ "GROUP BY po."+ PeriodOutgoingsTable.COLUMN_OUTGOING_ID),
					
	SELECT_ACTIVE_CATEGORIES("SELECT * "
							+" FROM "+ CategoryTable.NAME
							+" WHERE "+ CategoryTable.COLUMN_IS_ACTIVE +" = 1"),	
	
	SELECT_ACTIVE_OUTGOINGS("SELECT * " 
							+ "FROM "+ OutgoingTable.NAME
							+ " WHERE "+ OutgoingTable.COLUMN_IS_ACTIVE +" = 1"),
	
	SELECT_DEACTIVE_CATEGORIES("SELECT * "
			+" FROM "+ CategoryTable.NAME
			+" WHERE "+ CategoryTable.COLUMN_IS_ACTIVE +" = 0"),	

	SELECT_DEACTIVE_OUTGOINGS("SELECT * " 
			+ "FROM "+ OutgoingTable.NAME
			+ " WHERE "+ OutgoingTable.COLUMN_IS_ACTIVE +" = 0");
	
	private String _Query;
	
	private SelectQueries(String query) {
		this._Query = query;
	}
	
	@Override
	public String toString() {
		return _Query;
	}
}
