/**
 * 
 */
package se.birdcroft.ikassan.data.database;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * 
 * An abstract data-source class which contain all the feature
 * every data-source will have. Based on CRUD philosophy.
 * 
 * @author Mickael Ekroth
 * @version 
 * @param <TEntity> The Entity to work with
 */
public abstract class DataSource<TEntity> {
	
	private final SQLiteOpenHelper _DataBaseHelper;
	private  SQLiteDatabase _Database = null;
	
	/**
	 * Constructor which needs to implements when it's extends. 
	 * Create's the database helper connection. 
	 * 
	 * @param ctx The current context
	 */
	public DataSource(final Context ctx) {
		_DataBaseHelper = DatabaseHelper.getInstance(ctx);
	}

	/**
	 * Open up the database
	 * @throws SQLException when the database couldn't open
	 */
	public void open() throws SQLException {
		_Database = _DataBaseHelper.getWritableDatabase();
	}
	
	/**
	 * Closes the database helper
	 */
	public void close(){
		_DataBaseHelper.close();
	}
	
	/**
	 * @return the _Database
	 */
	public SQLiteDatabase getDatabase() {
		return _Database;
	}

	/**
	 * Checks if the entity exists or not. 
	 * 
	 * @return true if the id exists.
	 */
	protected boolean exists(long id) {
		return findById(id) != null ;
	}
	
	/**
	 * Finds a item by an id
	 * @param id The id to search after
	 * @return The found item / null if not found
	 */
	public abstract TEntity findById(long id);
	
	/**
	 * Find items by a specific id 
	 * @param id The searched id
	 * @return List of all found items / null if not found
	 */
	public abstract List<TEntity> findAllById(long id);
	
	/**
	 * Finds all items in the entity table.
	 * @return List of all found items / null if not found
	 */
	public abstract List<TEntity> findAll();
	
	/**
	 * Saves the item if it not exists and update it if exists
	 * @param item The item to insert/update.
	 * @return The saved item / null if something want wrong
	 */
	public abstract long save(TEntity item);
	
	/**
	 * Deletion of an item from the database.
	 * @param deleteItem The item to delete.
	 */
	public abstract void delete(TEntity deleteItem);
	
	/**
	 * Create an entity object of the incoming cursor which is loaded
	 * with data from database.
	 * @param item The item to create an entity.
	 * @return The created entity.
	 */
	protected abstract TEntity createEntity(Cursor item);
}
