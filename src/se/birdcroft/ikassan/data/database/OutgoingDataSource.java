/**
 * 
 */
package se.birdcroft.ikassan.data.database;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import se.birdcroft.ikassan.data.entities.Outgoing;

/**
 * @author MickeEkroth
 *
 */
public class OutgoingDataSource extends DataSource<Outgoing> {

	/**
	 * Default data-source constructor sends the context to super class.
	 * @param ctx The context
	 */
	public OutgoingDataSource(Context ctx) {
		super(ctx);
	}

	@Override
	public Outgoing findById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Outgoing> findAllById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Outgoing> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long save(Outgoing item) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void delete(Outgoing deleteItem) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected Outgoing createEntity(Cursor item) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
