package se.birdcroft.ikassan.data.database;

import java.util.ArrayList;
import java.util.List;

import se.birdcroft.ikassan.data.database.tables.CategoryTable;
import se.birdcroft.ikassan.data.entities.Category;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

public class CategoryDataSource extends DataSource<Category> {

	private final static String LOG = CategoryDataSource.class.getSimpleName();
	
	private String[] _TblColumns = {
			CategoryTable.COLUMN_ID,
			CategoryTable.COLUMN_NAME,
			CategoryTable.COLUMN_IS_ACTIVE
	};
	
	public CategoryDataSource(Context ctx) {
		super(ctx);
		// TODO Auto-generated constructor stub
	}

	
	@Override
	public Category findById(long id) {
		Log.d(LOG, "Find ID: "+ id);
		
		if (id <= 0) {
			return null;
		}
		Cursor cursor = getDatabase().query(CategoryTable.NAME, 
											_TblColumns, 
											CategoryTable.COLUMN_ID + " = " + id, 
											null, null, null, null);
		cursor.moveToFirst();
		return createEntity(cursor);
	}

	/**
	 * Not implemented 
	 */
	@Override
	public List<Category> findAllById(long id) {
		return null;
	}

	@Override
	public List<Category> findAll() {
		List<Category> categories = new ArrayList<Category>();
		
		Cursor cursor = getDatabase().rawQuery(SelectQueries.SELECT_ACTIVE_CATEGORIES.toString(), null);
		Log.d(LOG, "Find categories :"+ cursor.getCount());
		
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()){
			categories.add(createEntity(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		
		Log.d(LOG, "Categories :"+ categories.toString());
		
		return categories;
	}

	@Override
	public long save(Category item) {
		ContentValues values = new ContentValues();
		values.put(CategoryTable.COLUMN_NAME, item.getName());
		values.put(CategoryTable.COLUMN_IS_ACTIVE, item.isActive());
		
		if (exists(item.getId())) {
			Log.d(LOG, "Category exists: "+ item.getName());
			
			return getDatabase().update(CategoryTable.NAME, 
								 values, 
								 CategoryTable.COLUMN_ID + "= ?", 
								 new String[] { String.valueOf(item.getId()) });
		} else {
			Log.d(LOG, "Category createds: "+ item.getName());
			return getDatabase().insert(CategoryTable.NAME, null, values);
		}
	}

	@Override
	public void delete(Category deleteItem) {
		//Sets the category is active to false by set active = 0
		//This to have a chance to activated again.
		deleteItem.setActive(0);
		Log.d(LOG, "Deactivate category: "+ deleteItem.getName());
		
		save(deleteItem);
	}

	@Override
	protected Category createEntity(Cursor item) {
		Category category = new Category();
		
		category.setId(item.getLong(0)); // 0 = Column _id
		category.setName(item.getString(1)); // 1 = Column name
		category.setActive(item.getInt(2)); // 2 = Column is_active
		
		return category;
	}
}
