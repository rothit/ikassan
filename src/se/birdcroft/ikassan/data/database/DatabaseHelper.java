package se.birdcroft.ikassan.data.database;

import java.util.ArrayList;
import java.util.List;

import se.birdcroft.ikassan.data.database.tables.CategoryTable;
import se.birdcroft.ikassan.data.database.tables.OutgoingTable;
import se.birdcroft.ikassan.data.database.tables.PeriodOutgoingsTable;
import se.birdcroft.ikassan.data.database.tables.PeriodProductsTable;
import se.birdcroft.ikassan.data.database.tables.PeriodTable;
import se.birdcroft.ikassan.data.database.tables.ProductTable;
import se.birdcroft.ikassan.data.database.tables.Table;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {

	private final static String LOG = DatabaseHelper.class.getSimpleName();
	
	private final static String DATABASE_NAME = "ikassa.db";
	private final static int DATABASE_VERSION = 2;
	
	private static DatabaseHelper _Instance; 
	
	private List<Table> _Tables = new ArrayList <Table>();	
			
	private DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		
		_Tables.add(new CategoryTable());
		_Tables.add(new OutgoingTable());
		_Tables.add(new PeriodTable());
		_Tables.add(new ProductTable());
		_Tables.add(new PeriodOutgoingsTable());
		_Tables.add(new PeriodProductsTable());
	}

	/**
	 * Gets the current instance of this singleton object.
	 * 
	 * @param ctx Context 
	 * @return current created object.
	 */
	public static DatabaseHelper getInstance(Context ctx) {
		if(_Instance == null) {
			_Instance = new DatabaseHelper(ctx);
		}
		return _Instance;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		for(Table table : _Tables){
			Log.d(LOG, "DB Query: "+ table.create());
			Table.onCreate(db, table.create());
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		for(Table table : _Tables){
			Log.d(LOG, "Table: "+ table.getTableName());
			Table.onUpdate(db, table.getTableName(), oldVersion, newVersion);
		}
	}

}
