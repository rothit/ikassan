package se.birdcroft.ikassan.data.entities;

import java.util.Date;

public class Period {
	
	private long mId;
	
	private Date mPeriod;

	private double mIncome;
	
	/**
	 * Constructor call when to create new object of Period.
	 * 
	 * @param id The id of perid.
	 * @param period The period
	 * @param income The income for the period
	 */
	public Period(long id, Date period, double income) {
		this.mId = id;
		this.mPeriod = period;
		this.mIncome = income;
	}

	/**
	 * @return the income
	 */
	public double getIncome() {
		return mIncome;
	}

	/**
	 * @param income the income to set
	 */
	public void setIncome(double income) {
		this.mIncome = income;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return mId;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.mId = id;
	}

	/**
	 * @return the period
	 */
	public Date getPeriod() {
		return mPeriod;
	}

	/**
	 * @param period the period to set
	 */
	public void setPeriod(Date period) {
		this.mPeriod = period;
	}
}
