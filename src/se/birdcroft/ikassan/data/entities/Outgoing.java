/**
 * 
 */
package se.birdcroft.ikassan.data.entities;

/**
 * @author Mikael Ekroth
 *
 */
public class Outgoing {
	private long mId;
	
	private String mName;
	
	private double mAmount;

	private boolean mIsActive;
	
	
	
	/**
	 * @return the isActive
	 */
	public int isActive() {
		return mIsActive ? 1 : 0;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(int isActive) {
		this.mIsActive = isActive == 1 ? true : false;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return mId;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.mId = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.mName = name;
	}

	/**
	 * @return the amount
	 */
	public double getAmount() {
		return mAmount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(double amount) {
		this.mAmount = amount;
	}
	
	
	
}
