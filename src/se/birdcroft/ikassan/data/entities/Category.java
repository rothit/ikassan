package se.birdcroft.ikassan.data.entities;

public class Category {
	
	private long _Id;
	private String _Name;
	private int _IsActive; 
	
	public Category() { }
	
	/**
	 * @param name
	 */
	public Category(String name) {
		_Name = name;
		_IsActive = 1;
	}
	
	/**
	 * @param name
	 */
	public Category(String name, int isActive) {
		this._Name = name;
		this._IsActive = isActive;
	}
	
	
	
	/**
	 * @param id
	 * @param name
	 * @param _IsActive
	 */
	public Category(long id, String name, int isActive) {
		super();
		this._Id = id;
		this._Name = name;
		this._IsActive = isActive;
	}


	/**
	 * @return the isActive
	 */
	public int isActive() {
		return _IsActive;
	}
	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(int isActive) {
		_IsActive = isActive;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return _Id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this._Id = id;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return _Name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this._Name = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return _Name;
	}
	
	
}
