package se.birdcroft.ikassan.data.entities;

public class Product {
	
	private long mId;
	
	private String mName;
	
	private double mPrice;

	private long mCategoryId;

	/**
	 * @return the id
	 */
	public long getId() {
		return mId;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.mId = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return mName;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.mName = name;
	}

	/**
	 * @return the price
	 */
	public double getPrice() {
		return mPrice;
	}

	/**
	 * @param price the price to set
	 */
	public void setPrice(double price) {
		this.mPrice = price;
	}

	/**
	 * @return the categoryId
	 */
	public long getCategoryId() {
		return mCategoryId;
	}

	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(long categoryId) {
		this.mCategoryId = categoryId;
	}
	
	
	
}
