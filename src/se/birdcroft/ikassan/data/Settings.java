package se.birdcroft.ikassan.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Settings {
	public static final String APP_PREF = "ApplicationPrefs";
	
	public static final String PREF_CURRENT_PERIOD = "CurrentPeriod";
	public static final String PERF_PERIOD_BREAKPOINT = "PeriodBreakpoint";
	
	private SharedPreferences mSharedPrefs;
	private Editor mEditor;
	
	public Settings(Context context){
		this.mSharedPrefs = context.getSharedPreferences(APP_PREF, 0);
		this.mEditor = mSharedPrefs.edit();
	}

    public void setStringValue(String key, String value) {
        this.mEditor.putString(key, value);
        this.mEditor.commit();
    }
    
    public void setIntValue(String key, int value) {
        this.mEditor.putInt(key, value);
        this.mEditor.commit();
    }
    
    public String getStringValue(String key) {
        return this.mSharedPrefs.getString(key, null);
    }

    public int getIntValue(String key) {
        return this.mSharedPrefs.getInt(key, 0);
    }
    
    public void clear(String key) {
        this.mEditor.remove(key);
        this.mEditor.commit();
    }

    public void clear() {
        this.mEditor.clear();
        this.mEditor.commit();
    }
    
    public boolean isDefault(){
    	return this.mSharedPrefs.getBoolean("isDefault", true);
    }
}
